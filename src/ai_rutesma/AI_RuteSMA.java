
package ai_rutesma;

import java.awt.*;
import static java.awt.Color.YELLOW;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;
import java.util.Stack;
import javafx.scene.control.Cell;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.JComboBox;

/**
 *
 * @author Novanita
 */
public class AI_RuteSMA {

    /**
     * @param args the command line arguments
     */
    public static JFrame mazeFrame;
    static int r, c;

    public static void main(String[] args) {
//        try {
//                         UIManager.setLookAndFeel("com.jtattoo.plaf.noire.NoireLookAndFeel");
//	            
//        } catch (Exception e){
//          System.out.println(e);          
//        }
        int width  = 940;
        int height = 610;
        mazeFrame = new JFrame("Pencarian Rute Siswa Menuju SMA");
        mazeFrame.setContentPane(new MazePanel(width,height));
        mazeFrame.getContentPane().setBackground(new java.awt.Color(203,247,232));
        mazeFrame.pack();
        mazeFrame.setResizable(false);
        mazeFrame.setLocationRelativeTo(null);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double screenWidth = screenSize.getWidth();
        double ScreenHeight = screenSize.getHeight();
        int x = ((int)screenWidth-width)/2;
        int y = ((int)ScreenHeight-height)/2;

        mazeFrame.setLocation(x,y);
        mazeFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mazeFrame.setVisible(true);
    }
    
public static class MazePanel extends JPanel {
        private final static int
            INFINITY = Integer.MAX_VALUE, // The representation of the infinite
            EMPTY    = 0,  // empty cell
            OBST     = 1,  // cell with obstacle
            ROBOT    = 2,  // the position of the robot
            TARGET   = 3,  // the position of the target
            FRONTIER = 4,  // cells that form the frontier (OPEN SET)
            CLOSED   = 5,  // cells that form the CLOSED SET
            ROUTE    = 6, // cells that form the robot-to-target path
            TARGETFINAL = 7,
            FINAL=8;
        
        private final static String
            msgDrawAndSelect =
                "Tentukan posisi saat ini dan pom bensin TUJUAN",
            msgNoSolution =
                "Tidak dapat menuju pom bensin TUJUAN";
        
        JSpinner rowsSpinner, columnsSpinner; // Spinners for entering # of rows and columns
        
        int rows    = 37,           // the number of rows of the grid
            columns = 42,           // the number of columns of the grid
            squareSize = 620/rows;  // the cell size in pixels
        ArrayList<Cell> openSet   = new ArrayList();// the OPEN SET
        ArrayList<Cell> closedSet = new ArrayList();// the CLOSED SET
        ArrayList<Cell> graph     = new ArrayList();// the set of vertices of the graph
                                                    // to be explored by Dijkstra's algorithm
        Cell robotStart;
        Cell targetPos;
        JLabel message1, message2, imageLabel, startlabel, resetlabel;
        JButton clearButton, animationButton;
        JComboBox<String> Sekolah;
        JCheckBox hambatan;
        JPanel animPanel,animPanel1,batas;
        JSlider slider;
        JLabel pilihsma=new JLabel("PilihSekolah");
        JLabel sma1=new JLabel("SMAN 1");
        JLabel sma2=new JLabel("SMAN 2");
        JLabel sma3=new JLabel("SMAN 3");
        JLabel sma4=new JLabel("SMAN 4");
        JLabel sma5=new JLabel("SMAN 5");
        JLabel sma6=new JLabel("SMAN 6");
        JLabel sma7=new JLabel("SMAN 7");
        JLabel sma8=new JLabel("SMAN 8");
        JLabel smak=new JLabel("SMAN Kesuma");
        
        JComboBox algoritma;
        
        String[] tujuan={"pilih Sekolah", "SMAN 1", "SMAN 2", "SMAN 3", "SMAN 4",
                        "SMAN 5", "SMAN 6", "SMAN 7", "SMAN 8", "SMAN Kesuma"};
        JComboBox goal=new JComboBox(tujuan);
                
        int[][] grid;        // the grid
        boolean realTime;    // Solution is displayed instantly
        boolean found;       // flag that the goal was found
        boolean searching;   // flag that the search is in progress
        boolean endOfSearch; // flag that the search came to an end
        int delay;           // time delay of animation (in msec)
        int expanded;        // the number of nodes that have been expanded

        RepaintAction action = new RepaintAction();

        Timer timer;
        
        //Constructor
        public MazePanel(int width, int height) {
      
            setLayout(null);
            
            MouseHandler listener = new MouseHandler();
            addMouseListener(listener);
            addMouseMotionListener(listener);

            setBorder(BorderFactory.createMatteBorder(2,2,2,2,new java.awt.Color(203,247,232)));

            setPreferredSize( new Dimension(width,height) );

            grid = new int[rows][columns];

            message1 = new JLabel(msgDrawAndSelect, JLabel.CENTER);
            message1.setForeground(Color.ORANGE);
            message1.setFont(new Font("Tahoma",Font.PLAIN,20));
            
            batas=new JPanel();
            batas.setBackground(new java.awt.Color(140, 195, 221));
            
            message2 = new JLabel(msgDrawAndSelect, JLabel.CENTER);
            message2.setForeground(Color.ORANGE);
            message2.setFont(new Font("Tahoma",Font.PLAIN,20));

            hambatan = new JCheckBox("Hambatan");
            
            JLabel rowsLbl = new JLabel("# of rows (5-83):", JLabel.RIGHT);
            rowsLbl.setFont(new Font("Times New Roman",Font.PLAIN,13));

            SpinnerModel rowModel = new SpinnerNumberModel(71, //initial value
                                       5,  //min
                                       83, //max
                                       1); //step
            rowsSpinner = new JSpinner(rowModel);
 
            JLabel columnsLbl = new JLabel("# of columns (5-83):", JLabel.RIGHT);
            columnsLbl.setFont(new Font("Times New Roman",Font.PLAIN,13));

            SpinnerModel colModel = new SpinnerNumberModel(71, //initial value
                                       5,  //min
                                       83, //max
                                       1); //step
            columnsSpinner = new JSpinner(colModel);

            clearButton = new JButton("Hapus");
            clearButton.addActionListener(new ActionHandler());
            clearButton.setBackground(Color.lightGray);

            animationButton = new JButton("Rute(Animasi)");
            animationButton.addActionListener(new ActionHandler());
            animationButton.setBackground(Color.lightGray);
            animationButton.setToolTipText
                    ("Cari Rute Menuju Tujuan Menggunakan Animasi");

            JLabel velocity = new JLabel("Kecepatan", JLabel.CENTER);
            velocity.setFont(new Font("Times New Roman",Font.PLAIN,10));
            
            slider = new JSlider(0,1000,500); // initial value of delay 500 msec
            
            delay = 1000-slider.getValue();
            slider.addChangeListener((ChangeEvent evt) -> {
                JSlider source = (JSlider)evt.getSource();
                if (!source.getValueIsAdjusting()) {
                    delay = 1000-source.getValue();
                }
            });

            Sekolah = new JComboBox<>();
            Sekolah.setModel(new javax.swing.DefaultComboBoxModel<> (new String[]{"Sekolah", "SMAN 1", "SMAN 2", "SMAN 3", "SMAN 4", "SMAN 5", "SMAN 6", "SMAN 7", "SMAN 8", "SMAK Kesuma"}));
            Sekolah.addActionListener(new ActionHandler());
           
            JPanel algoPanel = new JPanel();
            algoPanel.setBorder(javax.swing.BorderFactory.
                    createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(),
                    "Menu", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                    javax.swing.border.TitledBorder.TOP, new java.awt.Font("Times New Roman", 0, 14)));
            
            animPanel = new JPanel();
            animPanel.setBorder(javax.swing.BorderFactory.
                    createTitledBorder(javax.swing.BorderFactory.createEmptyBorder(),
                    "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                    javax.swing.border.TitledBorder.LEFT, new java.awt.Font("Times New Roman", 0, 14)));
            animPanel.setBackground(new java.awt.Color(203,247,232));
            
            animPanel1 = new JPanel();
            animPanel1.setBorder(javax.swing.BorderFactory.
                    createTitledBorder(javax.swing.BorderFactory.createLineBorder(Color.GRAY),
                    "Keterangan", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                    javax.swing.border.TitledBorder.LEFT, new java.awt.Font("Helvetica", 0, 14)));
            animPanel1.setBackground(new java.awt.Color(203,247,232));
            
            //dfs.setSelected(true);  // DFS is initially selected 

            JLabel robot = new JLabel("POSISI", JLabel.CENTER);
            robot.setForeground(Color.red);
            robot.setFont(new Font("Helvetica",Font.PLAIN,14));
                
            algoritma=new javax.swing.JComboBox<>();
            algoritma.setFont(new java.awt.Font("Times New Roman", 0, 14));
            algoritma.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {"Metode","BFS", "DFS", "AStar"  }));
            
            JLabel target = new JLabel("SMA", JLabel.CENTER);
            target.setForeground(Color.GRAY);
            target.setFont(new Font("Helvetica",Font.PLAIN,14));
         
            JLabel frontier = new JLabel("EXPAND", JLabel.CENTER);
            frontier.setForeground(Color.blue);
            frontier.setFont(new Font("Helvetica",Font.PLAIN,14));
            
            imageLabel = new JLabel("", JLabel.CENTER);
            startlabel = new JLabel("",JLabel.CENTER);
            resetlabel = new JLabel("",JLabel.CENTER);

            add(hambatan);
            add(message1);
            add(batas);
            add(message2);
            add(clearButton);
            add(animationButton);
            add(velocity);
            add(algoritma);
            add(slider);
            add(Sekolah);
            add(algoPanel);
            add(robot);
            add(target);
            add(frontier);
            add(animPanel);
            add(animPanel1);
            add(imageLabel);
            add(startlabel);
            add(resetlabel);
            
            message1.setBounds(575, 325, 500, 23);
            message2.setBounds(640, 375, 500, 23);
            batas.setBounds(725, 360, 195, 3);
            velocity.setBounds(730, 85, 170, 10);
            slider.setBounds(730, 100, 170, 25);
            hambatan.setBounds(730, 130, 170, 25);
            Sekolah.setBounds(725, 10, 170, 25);
            robot.setBounds(720, 250, 80, 25);
            target.setBounds(815, 250, 80, 25);
            frontier.setBounds(720, 270, 80, 25);
            animPanel.setSize(240, 260);  //ganti pake gambar
            animPanel.setLocation(700, 415); //ganti pake gambar
            animPanel1.setSize(200, 80);  //ganti pake gambar
            animPanel1.setLocation(720, 230); //ganti pake gambar
            imageLabel.setBounds(35, 0, 0, 0); //ganti pake gambar
            startlabel.setBounds(730, 170, 66, 48);
            resetlabel.setBounds(820, 170, 75, 48);
            algoritma.setBounds(725, 50, 170, 25);
            timer = new Timer(delay, action);
            
            fillGrid();
            ruteSma();

        } // end constructor
        public boolean ai1(){
            try {
                ImageIcon ii = new ImageIcon(this.getClass().getResource(
                        "jalan.gif"));
                imageLabel.setIcon(ii);
                animPanel.add(imageLabel, java.awt.BorderLayout.CENTER);
                // show it
//                this.setVisible(true);
//                this.revalidate();
                repaint();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return false;
        }
        public boolean ai2(){
            try {
                ImageIcon ii = new ImageIcon(this.getClass().getResource(
                        "desain.jpg"));
                imageLabel.setIcon(ii);
                animPanel.add(imageLabel, java.awt.BorderLayout.CENTER);
                // show it
//                this.setLocationRelativeTo(null);
//                this.setVisible(true);
//                this.revalidate();
                repaint();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return false;
        }
        public boolean ai3(){
            try {
                ImageIcon ii = new ImageIcon(this.getClass().getResource(
                        "sampai11.gif"));
                imageLabel.setIcon(ii);
                animPanel.add(imageLabel, java.awt.BorderLayout.CENTER);
                // show it
//                this.setVisible(true);
//                this.revalidate();
                repaint();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return false;
        }
        public boolean mulai(){
            try {
                ImageIcon start = new ImageIcon(this.getClass().getResource(
                        "start.jpg"));
                startlabel.setIcon(start);
                startlabel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                startlabel.addMouseListener(new java.awt.event.MouseAdapter() {
                @Override
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                startlabelMouseClicked(evt);
                 }
                });
                // show it
//                this.setVisible(true);
//                this.revalidate();
                repaint();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return false;
            }
            public boolean kembali(){
            try {
                ImageIcon reset = new ImageIcon(this.getClass().getResource(
                        "reset.jpg"));
                resetlabel.setIcon(reset);
                resetlabel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                resetlabel.addMouseListener(new java.awt.event.MouseAdapter() {
                @Override
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                resetlabelMouseClicked(evt);
                 }
                });
                // show it
//                this.setVisible(true);
//                this.revalidate();
                repaint();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return false;
            }
        
        private void startlabelMouseClicked(java.awt.event.MouseEvent evt) {                                     
        // mulai
                    ai1();
                    realTime = false;
                    searching = true;
                    animationButton.setForeground(Color.green);
                    timer.setDelay(delay);
                    timer.start(); 
                    algoritma.setEnabled(false);
        }
        
        private void resetlabelMouseClicked(java.awt.event.MouseEvent evt) {                                     
        // kembali
                    fillGrid();
                    realTime = false;
                    animationButton.setForeground(Color.black);
                    animationButton.setEnabled(true);
                    slider.setEnabled(true);
                    algoritma.setEnabled(true);
        
        }
        private class Cell {
            int row; 
            int col;  
            int g;    
            int h;    
            int f;     
            Cell prev; 
            
            public Cell(int row, int col){
               this.row = row;
               this.col = col;
            }
        } // end nested class Cell

      
        private class MouseHandler implements MouseListener, MouseMotionListener {
            private int cur_row, cur_col, cur_val;
            @Override
            public void mousePressed(MouseEvent evt) {
                int row = (evt.getY() - 10) / squareSize;
                int col = (evt.getX() - 10) / squareSize;
                if (row >= 0 && row < rows && col >= 0 && col < columns) {
                    if (realTime ? true : !found && !searching){

                        if (realTime) {
                            searching = true;
                            fillGrid();
                        }
                        cur_row = row;
                        cur_col = col;
                        cur_val = grid[row][col]; 
                        if(hambatan.isSelected()){
                         if (cur_val == EMPTY){
                            grid[row][col] = OBST;
                        }
                        if (cur_val == OBST){
                            grid[row][col] = EMPTY;
                        }
                        }
                    }
                }
                if (realTime) {
                    timer.setDelay(0);
                    timer.start();
                    checkTermination();
                } else {
                    repaint();
                }
            }

            @Override
            public void mouseDragged(MouseEvent evt) {
                int row = (evt.getY() - 10) / squareSize;
                int col = (evt.getX() - 10) / squareSize;
                if (row >= 0 && row < rows && col >= 0 && col < columns){
                    if (realTime ? true : !found && !searching){
                        if (realTime) {
                            searching = true;
                            fillGrid();
                        }
                        if ((row*columns+col != cur_row*columns+cur_col) && (cur_val == ROBOT)){
                            int new_val = grid[row][col];
                            if (new_val == EMPTY){
                                grid[row][col] = cur_val;
                                if (cur_val == ROBOT) {
                                    robotStart.row = row;
                                    robotStart.col = col;
                                } 
                                grid[cur_row][cur_col] = new_val;
                                cur_row = row;
                                cur_col = col;
                                if (cur_val == ROBOT) {
                                    robotStart.row = cur_row;
                                    robotStart.col = cur_col;
                                } 
                                cur_val = grid[row][col];
                            }
                        } 
                       
                    }
                }
                if (realTime) {
                    timer.setDelay(0);
                    timer.start();
                    checkTermination();
                } else {
                    repaint();
                }
            }

            @Override
            public void mouseReleased(MouseEvent evt) { }
            @Override
            public void mouseEntered(MouseEvent evt) { }
            @Override
            public void mouseExited(MouseEvent evt) { }
            @Override
            public void mouseMoved(MouseEvent evt) { }
            @Override
            public void mouseClicked(MouseEvent evt) { }
            
        } // end nested class MouseHandler
        
        
        private class ActionHandler implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent evt) {
                String cmd = evt.getActionCommand();
                if (cmd.equals("Hapus")) {
                    fillGrid();
                    sma1.setVisible(true);
                    sma2.setVisible(true);
                    sma3.setVisible(true);
                    sma4.setVisible(true);
                    sma5.setVisible(true);
                    sma6.setVisible(true);
                    sma7.setVisible(true);
                    sma8.setVisible(true);
                    smak.setVisible(true);
                    realTime = false;
                    animationButton.setForeground(Color.black);
                    animationButton.setEnabled(true);
                    slider.setEnabled(true);
                } 
                  else if (cmd.equals("Rute(Animasi)") && !endOfSearch) {                   
                    ai1();
                    
                    sma1.setVisible(false);
                    sma2.setVisible(false);
                    sma3.setVisible(false);
                    sma4.setVisible(false);
                    sma5.setVisible(false);
                    sma6.setVisible(false);
                    sma7.setVisible(false);
                    sma8.setVisible(false);
                    smak.setVisible(false);
                    realTime = false;
                    searching = true;
                    animationButton.setForeground(Color.green);
                    timer.setDelay(delay);
                    timer.start(); 
                    
                    for (int i=0; i<rows; i++){
                        for(int j=0; j<columns;j++){
                            if(grid[i][j]==TARGET){
                                grid[i][j]=EMPTY;
                            }
                        }
                    }
                    
//                    String sklh=(String)Sekolah.getSelectedItem();
//                    
//                    switch (sklh) {
//                        case "SMAN 1":
//                            grid[16][14] = TARGETFINAL;
//                            targetPos = new Cell(16,14);
//                            break;
//                        case "SMAN 2":
//                            grid[25][6] = TARGETFINAL;
//                            targetPos = new Cell(25,6);
//                            break;
//                        case "SMAN 3":
//                            grid[22][10] = TARGETFINAL;
//                            targetPos = new Cell(22,10);
//                            break;
//                        case "SMAN 4":
//                            grid[28][28] = TARGETFINAL;
//                            targetPos = new Cell(28,28);
//                            break;
//                        case "SMAN 5":
//                            grid[10][17] = TARGETFINAL;
//                            targetPos = new Cell(10,17);
//                            break;
//                        case "SMAN 6":
//                            grid[11][40] = TARGETFINAL;
//                            targetPos = new Cell(11,40);
//                            break;
//                        case "SMAN 7":
//                            grid[1][11] = TARGETFINAL;
//                            targetPos = new Cell(1,11);
//                            break;
//                        case "SMAN 8":
//                            grid[32][27] = TARGETFINAL;
//                            targetPos = new Cell(32,27);
//                            break;
//                        case "SMAK KESUMA":
//                            grid[12][27] = TARGETFINAL;
//                            targetPos = new Cell(12,27);
//                            break;
//                        default:
//                            break;
//                    }             
                } 
            }
        } // end nested class ActionHandler
        
        public void ruteSma(){
            //baris 1
                        for(int i=0;i<42;i++){
                            if(i==1||i==21||i==34){
                                continue;
                            }
                            else {
                                grid[0][i]=OBST;
                            }
                        }
                        //baris 2
                        for(int i=0;i<42;i++){
                            if(i==0||i==41){
                                grid[1][i]=OBST;
                            }
                            else{
                                continue;
                            }
                        }
                        //baris 3
                        for(int i=0;i<42;i++){
                            if(i==1||i==17||i==21||i==27||i==34||i==40){
                                continue;
                            }
                            else {
                                grid[2][i]=OBST;
                            }
                        }
                        //baris 4
                        for(int i=0;i<42;i++){
                            if(i==1||i==17||i==21||i==27||i==34||i==40){
                                continue;
                            }
                                grid[3][i]=OBST;
                        }
                        //baris 5
                        for(int i=0;i<42;i++){
                            if(i==1||i==17||i==21||i==27||i==34||i==40){
                                continue;
                            }
                            else {
                                grid[4][i]=OBST;
                            }
                        }
                        //baris 6
                        for(int i=0;i<42;i++){
                            if(i==1||i==17||i==21||i==27||i==34||i==40){
                                continue;
                            }
                            else {
                                grid[5][i]=OBST;
                            }    
                        }
                        //baris 7
                        for(int i=0;i<42;i++){
                            if(i==1||i==17||i==21||i==27||i==34||i==40){
                                continue;
                            }
                            else {
                                grid[6][i]=OBST;
                            }
                        }
                         //baris 8
                        for(int i=0;i<42;i++){
                            if(i>0&&i<=4){
                                continue;
                            }
                            if(i==17||i>=21&&i<=42){
                                continue;
                            }
                            else {
                                grid[7][i]=OBST;
                            }
                        }
                         //baris 9
                        for(int i=0;i<42;i++){
                            if(i==1||i==4||i==17||i==21||i==34||i==40){
                                continue;
                            }
                            else {
                                grid[8][i]=OBST;
                            }
                        }
                         //baris 10
                        for(int i=0;i<42;i++){
                            if(i==1||i==4||i==17||i==21||i==34||i==40){
                                continue;
                            }
                            else {
                                grid[9][i]=OBST;
                            }
                        }
                        //baris 11
                        for(int i=0;i<42;i++){
                            if(i==1||i==4||i==17||i==21||i==34||i==40){
                                continue;
                            }
                            else {
                                grid[10][i]=OBST;
                            }
                        }
                        //baris 12
                        for(int i=0;i<42;i++){
                            if(i==1||i==4||i==17||i==21||i==34||i==40){
                                continue;
                            }
                            else {
                                grid[11][i]=OBST;
                            }
                        }
                        //baris 13
                        for(int i=0;i<42;i++){
                            if(i==1||i>=4&&i<35||i==40){
                                continue;
                            }
                            else {
                                grid[12][i]=OBST;
                            }
                        }
                        //baris 14
                        for(int i=0;i<42;i++){
                            if(i==1||i==8||i==17||i==22|i==34||i==40){
                                continue;
                            }
                            else {
                                grid[13][i]=OBST;
                            }
                        }
                        //baris 15
                        for(int i=0;i<42;i++){
                            if(i==1||i==8||i==17||i==22|i==34||i==40){
                                continue;
                            }
                            else {
                                grid[14][i]=OBST;
                            }
                        }
                        //baris 16
                        for(int i=0;i<42;i++){
                            if(i==1||i>=8&&i<=11||i==17||i==22|i==34||i==40){
                                continue;
                            }
                            else {
                                grid[15][i]=OBST;
                            }
                        }
                        //baris 17
                        for(int i=0;i<42;i++){
                            if(i==1||i==8||i>=11&&i<=34||i==40){
                                continue;
                            }
                            else {
                                grid[16][i]=OBST;
                            }
                            if(i==14){
                                grid[16][i]=EMPTY;
                            }
                        }
                        //baris 18
                        for(int i=0;i<42;i++){
                           if(i==1||i==8||i==16||i==22||i==25||i==30||i==34||i==40){
                                continue;
                            }
                            else {
                                grid[17][i]=OBST;
                            } 
                        }
                         //baris 19
                        for(int i=0;i<42;i++){
                            if(i==1||i==8||i==16||i==22||i==25||i==30||i==34||i==40){
                                continue;
                            }
                            else {
                                grid[18][i]=OBST;
                            } 
                        }
                         //baris 20
                        for(int i=0;i<42;i++){
                            if(i==1||i==8||i==16||i==22||i==25||i==30||i==34||i==40){
                                continue;
                            }
                            else {
                                grid[19][i]=OBST;
                            } 
                        }
                         //baris 21
                        for(int i=0;i<42;i++){
                           if(i==1||i==8||i==16||i==22||i==25||i==30||i==34||i>=35&&i<=42){
                                continue;
                            }
                            else {
                                grid[20][i]=OBST;
                            }
                        }
                         //baris 22
                        for(int i=0;i<42;i++){
                            if(i==1||i==8||i==16||i==22||i==25||i==30||i==40){
                                continue;
                            }
                            else {
                                grid[21][i]=OBST;
                            }
                        }
                         //baris 23
                        for(int i=0;i<42;i++){
                            if(i==0||i==41||i==42){
                                grid[22][i]=OBST;
                            }
                            else {
                                continue;
                            }
                        }
                         //baris 24
                        for(int i=0;i<42;i++){
                            if(i==1||i==6||i==16||i==25||i==30||i==40){
                                continue;
                            }
                            else {
                                grid[23][i]=OBST;
                            }
                        }
                         //baris 25
                        for(int i=0;i<42;i++){
                            if(i==1||i==6||i==16||i==25||i==30||i==40){
                                continue;
                            }
                            else {
                                grid[24][i]=OBST;
                            }
                        }
                         //baris 26
                        for(int i=0;i<42;i++){
                           if(i==1||i==6||i==16||i==25||i==30||i==40){
                                continue;
                            }
                            else {
                                grid[25][i]=OBST;
                            }
                        }
                         //baris 27
                        for(int i=0;i<42;i++){
                            if(i==1||i==6||i==16||i==25||i==30||i==40){
                                continue;
                            }
                            else {
                                grid[26][i]=OBST;
                            }
                        }
                         //baris 28
                        for(int i=0;i<42;i++){
                            if(i==1||i==6||i==16||i==25||i==30||i==40){
                                continue;
                            }
                            else {
                                grid[27][i]=OBST;
                            }
                        }
                         //baris 29
                        for(int i=0;i<42;i++){
                            if(i==1||i==6||i==16||i>=25&&i<=30||i==40){
                                continue;
                            }
                            else {
                                grid[28][i]=OBST;
                            }
                        }
                         //baris 30
                        for(int i=0;i<42;i++){
                            if(i==1||i==6||i==16||i==25||i==30||i==40){
                                continue;
                            }
                            else {
                                grid[29][i]=OBST;
                            }
                        }
                         //baris 31
                        for(int i=0;i<42;i++){
                            if(i==1||i==6||i==16||i==25||i==30||i==40){
                                continue;
                            }
                            else {
                                grid[30][i]=OBST;
                            }
                        }
                         //baris 32
                        for(int i=0;i<42;i++){
                            if(i==1||i==6||i==16||i==25||i==30||i==40){
                                continue;
                            }
                            else {
                                grid[31][i]=OBST;
                            }
                        }
                        //baris 33
                         for(int i=0;i<42;i++){
                            if(i==0){
                                grid[32][i]=OBST;
                            }
                        }
                         //baris 34
                         for(int i=0;i<42;i++){
                            if(i==16||i==30||i==40){
                                continue;
                            }
                            else {
                                grid[33][i]=OBST;
                            }
                        }
                         //baris 35
                         for(int i=0;i<42;i++){
                            if(i==16||i==30||i==40){
                                continue;
                            }
                            else {
                                grid[34][i]=OBST;
                            }
                        }
                         //baris 36
                         for(int i=0;i<42;i++){
                            if(i==16||i==30||i==40){
                                continue;
                            }
                            else {
                                grid[35][i]=OBST;
                            }
                        }
                          //baris 37
                         for(int i=0;i<42;i++){
                            if(i==16||i==30||i==40){
                                continue;
                            }
                            else {
                                grid[36][i]=OBST;
                            }
                        }
                        if (grid[1][11] == EMPTY){
                            grid[2][11] = TARGET;
                            targetPos = new Cell(1,11);
                            //sma7
                        }
                        if (grid[10][17] == EMPTY){
                            grid[10][18] = TARGET;
                            targetPos = new Cell(10,17);
                            //sma5
                        }
                        if (grid[11][40] == EMPTY){
                            grid[11][39] = TARGET;
                            targetPos = new Cell(11,40);
                            //sma6
                        }
                        if (grid[12][27] == EMPTY){
                            grid[13][27] = TARGET;
                            targetPos = new Cell(12,27);
                            //smak
                        }
                        if (grid[16][14] == EMPTY){
                            grid[17][14] = TARGET;
                            targetPos = new Cell(16,14);
                            //sma1
                        }
                        if (grid[22][10] == EMPTY){
                            grid[21][10] = TARGET;
                            targetPos = new Cell(22,10);
                            //sma3
                        }
                        if (grid[25][6] == EMPTY){
                            grid[25][7] = TARGET;
                            targetPos = new Cell(25,6);
                            //sma2
                        }
                        if (grid[28][28] == EMPTY){
                            grid[29][28] = TARGET;
                            targetPos = new Cell(28,28);
                            //sma4
                        }
                        if (grid[32][27] == EMPTY){
                            grid[31][27] = TARGET;
                            targetPos = new Cell(32,27);
                            //sma8
                        }
        }
        
        
        private void SekolahActionPerformed(java.awt.event.ActionEvent evt) {                                           
        if (Sekolah.getSelectedItem()=="Pilih SMA Tujuan"){
            Sekolah.requestFocus();
        }
        else{
                    String sklh=(String)Sekolah.getSelectedItem();
                    
                    switch (sklh) {
                        case "SMAN 1":
                            grid[16][14] = TARGETFINAL;
                            targetPos = new Cell(16,14);
                            break;
                        case "SMAN 2":
                            grid[25][6] = TARGETFINAL;
                            targetPos = new Cell(25,6);
                            break;
                        case "SMAN 3":
                            grid[22][10] = TARGETFINAL;
                            targetPos = new Cell(22,10);
                            break;
                        case "SMAN 4":
                            grid[28][28] = TARGETFINAL;
                            targetPos = new Cell(28,28);
                            break;
                        case "SMAN 5":
                            grid[10][17] = TARGETFINAL;
                            targetPos = new Cell(10,17);
                            break;
                        case "SMAN 6":
                            grid[11][40] = TARGETFINAL;
                            targetPos = new Cell(11,40);
                            break;
                        case "SMAN 7":
                            grid[1][11] = TARGETFINAL;
                            targetPos = new Cell(1,11);
                            break;
                        case "SMAN 8":
                            grid[32][27] = TARGETFINAL;
                            targetPos = new Cell(32,27);
                            break;
                        case "SMAK KESUMA":
                            grid[12][27] = TARGETFINAL;
                            targetPos = new Cell(12,27);
                            break;
                        default:
                            break;
                    }             

        }
        } 
        
        private class RepaintAction implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent evt) {
                checkTermination();
                if (found) {
                    timer.stop();
                }
                if (!realTime) {
                    String sklh=(String)Sekolah.getSelectedItem();
                    
                    switch (sklh) {
                        case "SMAN 1":
                            grid[16][14] = TARGETFINAL;
                            targetPos = new Cell(16,14);
                            break;
                        case "SMAN 2":
                            grid[25][6] = TARGETFINAL;
                            targetPos = new Cell(25,6);
                            break;
                        case "SMAN 3":
                            grid[22][10] = TARGETFINAL;
                            targetPos = new Cell(22,10);
                            break;
                        case "SMAN 4":
                            grid[28][28] = TARGETFINAL;
                            targetPos = new Cell(28,28);
                            break;
                        case "SMAN 5":
                            grid[10][17] = TARGETFINAL;
                            targetPos = new Cell(10,17);
                            break;
                        case "SMAN 6":
                            grid[11][40] = TARGETFINAL;
                            targetPos = new Cell(11,40);
                            break;
                        case "SMAN 7":
                            grid[1][11] = TARGETFINAL;
                            targetPos = new Cell(1,11);
                            break;
                        case "SMAN 8":
                            grid[32][27] = TARGETFINAL;
                            targetPos = new Cell(32,27);
                            break;
                        case "SMAK KESUMA":
                            grid[12][27] = TARGETFINAL;
                            targetPos = new Cell(12,27);
                            break;
                        default:
                            break;
                    }             
                    repaint();
                }
            }
        } // end nested class RepaintAction
      
        public void checkTermination() {
                expandNode();
                if (found) {
                    endOfSearch = true;
                    plotRoute();
                    
                    animationButton.setEnabled(false);
                    slider.setEnabled(false);
                    repaint();
                    ai3();
                }
        }
        //3 d 2 b 1 a      
        private class CellComparatorByF implements Comparator<Cell>{
            @Override
            public int compare(Cell cell1, Cell cell2){
                return cell1.f-cell2.f;
            }
        } // end nested class CellComparatorByF
        private void expandNode(){
            Cell current;
            if (("DFS").equals(algoritma.getSelectedItem()) || ("BFS").equals(algoritma.getSelectedItem())) {
                current = openSet.remove(0);
            } else {
                Collections.sort(openSet, new CellComparatorByF());
                current = openSet.remove(0);
            }

            closedSet.add(0,current);
            grid[current.row][current.col] = CLOSED;

            if (current.row == targetPos.row && current.col == targetPos.col) {
                Cell last = targetPos;
                last.prev = current.prev;
                closedSet.add(last);
                found = true;
                return;
            } 
            expanded++;

            ArrayList<Cell> succesors;
            succesors = createSuccesors(current, false);
            succesors.stream().forEach((cell) -> {
                if (("DFS").equals(algoritma.getSelectedItem())) {
                    openSet.add(0, cell);
                    grid[cell.row][cell.col] = FRONTIER;
                } else if (("BFS").equals(algoritma.getSelectedItem())){
                    openSet.add(cell);
                    grid[cell.row][cell.col] = FRONTIER;
                } else if (("AStar").equals(algoritma.getSelectedItem())){
                    int dxg = current.col-cell.col;
                    int dyg = current.row-cell.row;
                    int dxh = targetPos.col-cell.col;
                    int dyh = targetPos.row-cell.row;
                    cell.g = current.g+Math.abs(dxg)+Math.abs(dyg);
                    cell.h = Math.abs(dxh)+Math.abs(dyh);
                    cell.f = cell.g+cell.h;
                    int openIndex   = isInList(openSet,cell);
                    int closedIndex = isInList(closedSet,cell);
                    if (openIndex == -1 && closedIndex == -1) {
                        openSet.add(cell);
                        grid[cell.row][cell.col] = FRONTIER;
                    } else {
                        if (openIndex > -1){
                            if (openSet.get(openIndex).f <= cell.f) {
                            } else {
                                openSet.remove(openIndex);
                                openSet.add(cell);
                                grid[cell.row][cell.col] = FRONTIER;
                            }
//                        } else {
//                            if (closedSet.get(closedIndex).f <= cell.f) {
//                            } else {
//                                closedSet.remove(closedIndex);
//                                openSet.add(cell);
//                                grid[cell.row][cell.col] = FRONTIER;
//                            }
                        }
                    }
                }
            });
        }
                
        private ArrayList<Cell> createSuccesors(Cell current, boolean makeConnected){
            int r = current.row;
            int c = current.col;
            ArrayList<Cell> temp = new ArrayList<>();

            if (r > 0 && grid[r-1][c] != OBST &&
                    ((("AStar").equals(algoritma.getSelectedItem())) ? true :
                          isInList(openSet,new Cell(r-1,c)) == -1 &&
                          isInList(closedSet,new Cell(r-1,c)) == -1)) {
                Cell cell = new Cell(r-1,c);
                cell.prev = current;
                temp.add(cell);
            }
            if (c < columns-1 && grid[r][c+1] != OBST &&
                    ((("AStar").equals(algoritma.getSelectedItem()))? true :
                          isInList(openSet,new Cell(r,c+1)) == -1 &&
                          isInList(closedSet,new Cell(r,c+1)) == -1)) {
                Cell cell = new Cell(r,c+1);
                cell.prev = current;
                temp.add(cell);
            }

            if (r < rows-1 && grid[r+1][c] != OBST &&
                    ((("AStar").equals(algoritma.getSelectedItem())) ? true :
                          isInList(openSet,new Cell(r+1,c)) == -1 &&
                          isInList(closedSet,new Cell(r+1,c)) == -1)) {
                Cell cell = new Cell(r+1,c);
                cell.prev = current;
                temp.add(cell);
            }

            if (c > 0 && grid[r][c-1] != OBST && 
                    ((("AStar").equals(algoritma.getSelectedItem())) ? true :
                          isInList(openSet,new Cell(r,c-1)) == -1 &&
                          isInList(closedSet,new Cell(r,c-1)) == -1)) {
                Cell cell = new Cell(r,c-1);
                cell.prev = current;
                temp.add(cell);
            }

            if (("DFS").equals(algoritma.getSelectedItem())){
                Collections.reverse(temp);
            }
            return temp;
        } // end createSuccesors()
        
        private int isInList(ArrayList<Cell> list, Cell current){
            int index = -1;
            for (int i = 0 ; i < list.size(); i++) {
                if (current.row == list.get(i).row && current.col == list.get(i).col) {
                    index = i;
                    break;
                }
            }
            return index;
        } // end isInList()

        private void plotRoute(){
            searching = false;
            endOfSearch = true;
            int steps = 0;
            double distance = 0;
            int index = isInList(closedSet,targetPos);
            Cell cur = closedSet.get(index);
            grid[cur.row][cur.col]= TARGETFINAL;
            do {
                steps++;
                 
                    distance++;
                
                cur = cur.prev;
                grid[cur.row][cur.col] = ROUTE;
            } while (!(cur.row == robotStart.row && cur.col == robotStart.col));
            grid[robotStart.row][robotStart.col]=ROBOT;
            String msg1,msg2;
            msg1 = String.format("Node expanded: %d",expanded);
            msg2 = String.format("%.2f KM",(distance*0.2));
            message1.setText(msg1);
            message2.setText(msg2);
          
        } // end plotRoute()

        private void fillGrid() {
            if (searching || endOfSearch){ 
                for (int r = 0; r < rows; r++) {
                    for (int c = 0; c < columns; c++) {
                        if (grid[r][c] == FRONTIER || grid[r][c] == CLOSED || grid[r][c] == ROUTE) {
                            grid[r][c] = EMPTY;
                        }
                        if (grid[r][c] == ROBOT){
                            robotStart = new Cell(r,c);
                        }
                        if(grid[r][c]==TARGETFINAL){
                            targetPos=new Cell(r,c);
                        }
                    }
                }
                searching = false;
            } else {
                for (int r = 0; r < rows; r++) {
                    for (int c = 0; c < columns; c++) {
                        grid[r][c] = EMPTY;
                        targetPos=null;
                    }
                }
                ruteSma();
                robotStart = new Cell(rows-5,1);
            }
            if (("DFS").equals(algoritma.getSelectedItem())){
                robotStart.g = 0;
                robotStart.h = 0;
                robotStart.f = 0;
            }
            else if (("BFS").equals(algoritma.getSelectedItem())){
                robotStart.g = 0;
                robotStart.h = 0;
                robotStart.f = 0;
            }
            else if (("AStar").equals(algoritma.getSelectedItem())){
                robotStart.g = 0;
                robotStart.h = 0;
                robotStart.f = 0;
            }
            expanded = 0;
            found = false;
            searching = false;
            endOfSearch = false;
         
            openSet.removeAll(openSet);
            
            
            openSet.add(robotStart);
            closedSet.removeAll(closedSet);
            grid[robotStart.row][robotStart.col] = ROBOT;
            message1.setText("Node yang Ditempuh");
            message2.setText("Jarak");
            timer.stop();
            repaint();
            ai2();
            mulai();
            kembali();
            
        } // end fillGrid()
        

        @Override
        public void paintComponent(Graphics g) {

            super.paintComponent(g);  // Fills the background color.

            g.setColor(Color.WHITE);
            
            g.fillRect(10, 10, columns*squareSize+1, rows*squareSize+1);
            
            for (int r = 0; r < rows; r++) {
                for (int c = 0; c < columns; c++) {
                    if (grid[r][c] == EMPTY) {
                        g.setColor(Color.WHITE);
                    } else if (grid[r][c] == ROBOT) {
                        g.setColor(Color.RED);
                    } else if (grid[r][c] == TARGET) {
                        g.setColor(Color.GRAY);
                    } else if (grid[r][c] == OBST) {
                        g.setColor(Color.BLACK);
                    } else if (grid[r][c] == FRONTIER) {
                        g.setColor(Color.BLUE);
//                        if(r==1 && c==31){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==5 && c==3){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==5 && c==41){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==10 && c==27){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==12 && c==37){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==17 && c==14){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==26 && c==14){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==28 && c==30){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==31 && c==41){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==35 && c==16){
//                            g.setColor(Color.RED);
//                        }
                    } else if (grid[r][c] == CLOSED) {
                        g.setColor(Color.CYAN);
//                        if(r==1 && c==31){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==5 && c==3){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==5 && c==41){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==10 && c==27){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==12 && c==37){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==17 && c==14){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==26 && c==14){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==28 && c==30){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==31 && c==41){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==35 && c==16){
//                            g.setColor(Color.RED);
//                        }
                    } else if (grid[r][c] == ROUTE) {
                        g.setColor(Color.YELLOW);
//                        if(r==1 && c==31){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==5 && c==3){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==5 && c==41){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==10 && c==27){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==12 && c==37){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==17 && c==14){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==26 && c==14){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==28 && c==30){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==31 && c==41){
//                            g.setColor(Color.RED);
//                        }
//                        if(r==35 && c==16){
//                            g.setColor(Color.RED);
//                        }
                    }
                    
                    g.fillRect(11 + c*squareSize, 11 + r*squareSize, squareSize , squareSize);
                }
                
                
            }
        }    
}

}
